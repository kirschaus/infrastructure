
from apiclient.discovery import build
from google.oauth2 import service_account
from flask import Flask, request, json, render_template
from google.cloud import datastore
from meraki import *
from datetime import datetime
import timeago
import json
import uuid

app = Flask(__name__)

datastore_client = datastore.Client('kirschaus-215003')
SCOPES = ["https://www.googleapis.com/auth/admin.directory.user"]
SERVICE_ACCOUNT_FILE = "/Users/kurtkirschner/kirschaus/infrastructure/compute.json"

credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)

dcredentials = credentials.with_subject('kurt@kirschaus.com')
service = build('admin', 'directory_v1', credentials=dcredentials)
userDirectory = (service.users())


def userList():
    results = (userDirectory.list(domain='kirschaus.com', orderBy='email').execute())
    users = (results['users'])
    return users

def userGet(idt):
    user = service.users().get(userKey=idt).execute()
    return user
'''
@app.route("/<idt>")
def idts(idt):
    #idt = 'abby@marmar.is'
    response = (userGet(idt))
    email = str(response['id'])
    return render_template("nav.html", nav_title='authenticate', nav_href='/', entity_name=email)
'''
@app.route('/inf/node')
def nodeConnect(idt):
    idt = request.args.get('idt')
    spc = request.args.get('spc')
    macs = idt['macs']
    ipv4 = idt['ipv4']
    hostnamne = idt['hostname']
    nclass = spc['class']
    ntype = spc['type']
    query = datastore_client.query(kind='Operand')
    query.add_filter('type', '=', 'node')
    query.add_filter('class', '=', 'raspberrypi')
    oprs = list(query.fetch())
    for opr in oprs:
        omacs = opr['idt']['macs']
        for tmac in macs:
            if tmac in omacs:
                break
            print('OPER')
    return response

@app.route('/dir/usr/')
def dirUsrs():
    response = Response(response=json.dumps(userList()),status=200,mimetype='application/json')
    return response
    
    
    '''
    
     response = (userGet(idt))
        email = str(response['id'])
    '''


datastore_client = datastore.Client('kirschaus-215003')

def endpointInit(wmac,mid):
    print(wmac,'wmac',mid,'mid')
    query = datastore_client.query(kind='Operand')
    results = list(query.fetch(limit=10))

def smDevices():
    init = datetime.now().timestamp()
    devices = getsmdevices(apikey,networkid)['devices']
    term = datetime.now().timestamp()
    for device in devices:
        endpointInit(device['wifiMac'],device['id'])
    smd = {'init':init,'term':term,'devices':devices}
    return smd



@app.route("/<idt>")
def index(idt):
    #idt = 'abby@marmar.is'
    response = (userGet('kurt@kirschaus.com'))
    query = datastore_client.query(kind='Endpoint')
    results = list(query.fetch())
    print(response)
    return render_template("base.html", title=idt,nodes=results)
if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='0.0.0.0', port=8080, debug=True)

